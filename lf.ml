type expreg =
  | Vide
  | Epsilon
  | Caractere of char
  | Union of expreg * expreg
  | Produit of expreg * expreg
  | Etoile of expreg

let rec contient_epsilon e =
  match e with
  | Vide -> false
  | Epsilon -> true
  | Caractere x -> false
  | Union (e1,e2) -> contient_epsilon e1 || contient_epsilon e2
  | Produit (e1,e2) -> contient_epsilon e1 && contient_epsilon e2
  | Etoile e -> true

let rec residu e c =
  match e with
  | Epsilon | Vide -> Vide
  | Caractere x ->
    if x = c then
      Epsilon
    else
      Vide
  | Union (e1,e2) -> 
    Union(residu e1 c,residu e2 c)
  | Produit (e1,e2) ->
    if contient_epsilon e1 then
      Union(
        residu e2 c,
        Produit(residu e1 c,e2)
      )
    else
      Produit(residu e1 c,e2)
  | Etoile e ->
    Produit (residu e c, Etoile e)

let rec reconnait e l =
  match l with
  | [] -> contient_epsilon e 
  | x :: s -> reconnait (residu e x) s

(* Fonctios d'affichages *)
let rec est_vide expr =    
  match expr with
  | Vide -> true
  | _ -> false

let rec est_epsilon expr =
  match expr with
  | Epsilon -> true
  | _ -> false

let rec clean expr =
  match expr with
  | Caractere _
  | Vide
  | Epsilon -> expr
  | Union (expr1,expr2) -> 
    let expr1 = clean expr1 in
    let expr2 = clean expr2 in
    if est_vide expr1 then
      expr2
    else if est_vide expr2 then
      expr1
    else
      Union(expr1,expr2)
  | Produit (expr1,expr2) ->
    let expr1 = clean expr1 in
    let expr2 = clean expr2 in
    if est_vide expr1 || est_vide expr2 then  
      Vide
    else if est_epsilon expr1 then
      expr2
    else if est_epsilon expr2 then
      expr1
    else
      Produit(expr1,expr2)
  | Etoile expr ->
    let expr = clean expr in
    if est_vide expr then
      Vide
    else if est_epsilon expr then
      Epsilon
    else
      Etoile( expr)

let to_string e =
  let cleaned_expr = clean e in
  let open String in

  let rec aux expr =
    match expr with
    | Vide -> "Vide"
    | Epsilon -> "ε"
    | Caractere c ->  String.make 1 c
    | Produit (expr1,expr2) ->
      "("^ (aux expr1) ^ " . " ^ (aux expr2) ^")"
    | Union (expr1,expr2) ->
      "("^ (aux expr1) ^ " + " ^ (aux expr2) ^")"
    | Etoile (expr1) ->
      "("^ (aux expr1) ^ ")*"
  in
  aux cleaned_expr
(* Fin des fonctions d'affichages *)

let () =
  let expr = Produit (
      Etoile (
        Union (
          Caractere('a'),
          Caractere('b')
        )
      ),
      Caractere('a')
    ) in
  let residu_par_a = residu expr 'a' in
  let residu_par_b = residu expr 'b' in

  print_string (to_string expr);
  print_newline ();
  print_string (to_string residu_par_a);
  print_newline ();
  print_string (to_string residu_par_b);
  print_newline ();

  Printf.printf "%B\n" (reconnait expr ['a';'b';'a'])
